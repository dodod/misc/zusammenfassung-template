TEX = pdflatex -interaction nonstopmode
GS = gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite 

PAPER = main.tex

all: $(PAPER)
	$(TEX) $(PAPER)
	$(TEX) $(PAPER)

view: $(PAPER)
	open $(PAPER)

spell::
	ispell *.tex

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof $(PAPER).pdf $(SUPP).pdf $(BUNDLE)
